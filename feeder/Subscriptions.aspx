﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Subscriptions.aspx.cs" Inherits="Subscriptions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p>
            Welcome <asp:Label ID="labelFriendlyUserName" runat="server" Text="" />
        </p>
    <p>
        <b>Subscriptions</b>
        <asp:Label ID="labelSubscriptions" runat="server" Text="" />
    </p>
        <b>Add Subscription</b>
        <p>Enter URL
            <asp:TextBox ID="textboxURL" runat="server" />
            <br />
        </p>
        <p>
            <asp:Button Text="Submit" runat="server" ID="buttonSubmit" OnClick="buttonSubmit_Click" />

        </p>
        <p>
            <asp:Label ID="labelSubmitSubscriptionStatus" runat="server" Text="" />
        </p>
    </div>
    </form>
</body>
</html>
