﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;

public partial class Subscriptions : System.Web.UI.Page
{
    int useridx
    {
        set;
        get;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       // labelSubmitSubscriptionStatus.Text = "";
       // labelSubmitSubscriptionStatus.DataBind();

        string username = HttpContext.Current.User.Identity.Name;

        string friendlyName;
        

        try
        {
            friendlyName = (string)Session["FriendlyLoginName"];
            labelFriendlyUserName.Text = friendlyName;

            useridx = System.Convert.ToInt32(Session["useridx"]);
            labelSubscriptions.Text = Users.getSubscriptions(useridx);
        }
        catch
        {
        
        }

        //populate label subscriptions
        Users.getSubscriptions(useridx);
       
    }



    protected void buttonSubmit_Click(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(textboxURL.Text) == false)
        {

            bool status = Subscription.AddSubscriptionForUser(textboxURL.Text, useridx);
            textboxURL.Text = "";
            textboxURL.DataBind();
        }
        else
        {
            labelSubmitSubscriptionStatus.Text = "Entry is null or empty!";
            labelSubmitSubscriptionStatus.DataBind();
        }
    }
}