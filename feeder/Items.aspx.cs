﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SubscriptionItems : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Pull subscription items and add them here
        int id = -1;


        if (Request.QueryString["id"] != null)
        {
            try
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }
            catch
            {
                id = -1;
            }
        }

        if (id != -1)
        {
            labelSubscriptionItems.Text = ParseRSS.getItems(id);
            labelSubscriptionItems.DataBind();
        }
        else
            Response.Redirect("Subscriptions.aspx");

    }
}