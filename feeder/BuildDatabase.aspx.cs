﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

public partial class BuildDatabase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string connectionString = ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString;
        IDbConnection dbcon = new MySqlConnection(connectionString);

        try
        {
            dbcon.Open();
            IDbCommand dbcmd = dbcon.CreateCommand();

            //table info
            //User: id, name, passphrase
            //Subscription: id, title, link, description
            //Item:  id, title, link, description, guid = guid + link, enclosure
            //UserSub: id, userid, subscriptionid
            //UserItem: id, userid, subscriptionid, itemid, itemstate [read, notread, saved e.t.c]
            
            
            string sql = "CREATE TABLE IF NOT EXISTS users ";
            sql += "(id SERIAL PRIMARY KEY AUTO_INCREMENT NOT NULL, ";
            sql += "userid TEXT NOT NULL, ";
            sql += "friendlyname TEXT NOT NULL, ";
            sql += "useremail TEXT NOT NULL, ";
            sql += "details TEXT NOT NULL) ENGINE=InnoDB;";

            dbcmd.CommandText = sql;
            dbcmd.ExecuteNonQuery();

            sql = "CREATE TABLE IF NOT EXISTS subscriptions ";
            sql += "(id SERIAL PRIMARY KEY AUTO_INCREMENT NOT NULL, ";
            sql += "title TEXT NOT NULL, ";
            sql += "weblink TEXT NOT NULL, ";
            sql += "feedlink TEXT NOT NULL, ";
            sql += "description TEXT NOT NULL) ENGINE=InnoDB;";

            dbcmd.CommandText = sql;
            dbcmd.ExecuteNonQuery();

            sql = "CREATE TABLE IF NOT EXISTS items ";
            sql += "(id SERIAL PRIMARY KEY AUTO_INCREMENT NOT NULL, ";
            sql += "subscriptionid BIGINT UNSIGNED NOT NULL, ";
            sql += "title TEXT NOT NULL, ";
            sql += "link TEXT NOT NULL, ";
            sql += "description TEXT NOT NULL, ";
            sql += "guid TEXT NOT NULL, ";
            sql += "enclosure TEXT NOT NULL, ";
            sql += "status TEXT NOT NULL, ";
            sql += "CONSTRAINT FK_subsitems FOREIGN KEY (subscriptionid) REFERENCES subscriptions(id) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;";

            dbcmd.CommandText = sql;
            dbcmd.ExecuteNonQuery();

            //UserSub: id, userid, subscriptionid
            //UserItem: id, userid (ref), itemid (ref), itemstate [read, notread, saved e.t.c]
            sql = "CREATE TABLE IF NOT EXISTS usersubs ";
            sql += "(id SERIAL PRIMARY KEY AUTO_INCREMENT NOT NULL, ";
            sql += "userid BIGINT UNSIGNED NOT NULL, ";
            sql += "subscriptionid BIGINT UNSIGNED NOT NULL, ";
            sql += "CONSTRAINT FK_usersubs_userid FOREIGN KEY (userid) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE, ";
            sql += "CONSTRAINT FK_usersubs_subsid FOREIGN KEY (subscriptionid) REFERENCES subscriptions(id) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;";

            dbcmd.CommandText = sql;
            dbcmd.ExecuteNonQuery();

            sql = "CREATE TABLE IF NOT EXISTS useritems ";
            sql += "(id SERIAL PRIMARY KEY AUTO_INCREMENT NOT NULL, ";
            sql += "userid BIGINT UNSIGNED NOT NULL, ";
            sql += "itemid BIGINT UNSIGNED NOT NULL, ";
            sql += "itemstatus TEXT NOT NULL, ";
            sql += "CONSTRAINT FK_useritems_userid FOREIGN KEY (userid) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE, ";
            sql += "CONSTRAINT FK_useritems_itemid FOREIGN KEY (itemid) REFERENCES items(id) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;";

            dbcmd.CommandText = sql;
            dbcmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            if (dbcon != null)
            {
                dbcon.Close();
            }
        }

    }
}