﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using Argotic.Common;
using Argotic.Syndication;

/// <summary>
/// Summary description for Subscriptions
/// </summary>
public class Subscription
{
	public Subscription()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static void AddFeedToDb(string url)
    {
        // string status = "Failed!";
        string title = "";
        string weblink = "";
        string feedlink = "";
        string description = "";

        string connectionString = ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString;
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);

        try
        {
            System.Uri Url = new Uri(url);

            GenericSyndicationFeed feed = GenericSyndicationFeed.Create(Url);

            SyndicationContentFormat format = feed.Format;

            feedlink = url;
            title = feed.Title;
            description = feed.Description;

            //DateTime lastUpdatedOn = feed.LastUpdatedOn;

            //foreach (GenericSyndicationCategory category in feed.Categories)
            // {
            //     string term = category.Term;
            //     string scheme = category.Scheme;
            // }

            //foreach (GenericSyndicationItem item in feed.Items)
            //{
            //    string title = item.Title;
            //    string summary = item.Summary;
            //    DateTime publishedOn = item.PublishedOn;

            //    foreach (GenericSyndicationCategory category in feed.Categories)
            //    {
            //        string itemCategoryTerm = category.Term;
            //        string itemCategoryTScheme = category.Scheme;
            //    }
            //}

            if (feed.Format == SyndicationContentFormat.Rss)
            {
                RssFeed rssFeed = feed.Resource as RssFeed;
                if (rssFeed != null)
                {
                    weblink = rssFeed.Channel.Link.AbsoluteUri;
                    // foreach (RssItem item in rssFeed.Channel.Items)
                    // {
                    // Process format specific information
                    //      RssGuid guid = item.Guid;
                    //      string sguid = guid.Value;
                    //     string desc = item.Description;
                    //   }
                }
            }
            if (feed.Format == SyndicationContentFormat.Atom)
            {
                AtomFeed atomFeed = feed.Resource as AtomFeed;

                if (atomFeed != null)
                {
                    foreach (AtomLink link in atomFeed.Links)
                    {
                        weblink = link.Uri.AbsolutePath;
                        break; //Just grab first link
                    }
                }
            }

            //Insert into DB
            string sql = "INSERT INTO subscriptions VALUES (DEFAULT, @title, @weblink, @feedlink, @description)";

            MySqlParameter sqltitle = new MySqlParameter("title", MySqlDbType.Text);
            sqltitle.Value = HttpUtility.HtmlEncode(title);

            MySqlParameter sqlweblink = new MySqlParameter("weblink", MySqlDbType.Text);
            sqlweblink.Value = HttpUtility.HtmlEncode(weblink);

            MySqlParameter sqlfeedlink = new MySqlParameter("feedlink", MySqlDbType.Text);
            sqlfeedlink.Value = HttpUtility.HtmlEncode(feedlink);

            MySqlParameter sqldescription = new MySqlParameter("description", MySqlDbType.Text);
            sqldescription.Value = HttpUtility.HtmlEncode(description);

            dbcon.Open();
            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.Parameters.Add(sqltitle);
            dbcmd.Parameters.Add(sqlweblink);
            dbcmd.Parameters.Add(sqlfeedlink);
            dbcmd.Parameters.Add(sqldescription);

            dbcmd.CommandText = sql;
            dbcmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            dbcon.Close();
        }

    }

    public static bool AddSubscriptionForUser(string url, int useridx)
    {

        //Check if subscription exists in subscriptions table.
        bool status = Subscription.doesFeedExistInDB(url);

        //If subscription not exist
        if (status == false)
        {
            Subscription.AddFeedToDb(url);

        }

        //Check if user is already subscribed
        status = Users.CheckIfUserIsSubscribedToAFeed(url, useridx);

        if (status == false)
        {
            try
            {
                Users.AddFeedToUserSubscriptions(url, useridx);
            }
            catch
            {
                status = false;
            }
        } //user is not subscribed, add to usersubs table
        return (status);
    }

    public static bool doesFeedExistInDB(string url)
    {
        bool status = false;
        url = url.Trim();


        string connectionString = ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString;
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);

        try
        {
            string sql = "SELECT COUNT(feedlink) FROM subscriptions WHERE feedlink=@feedlink";


            MySqlParameter sqlfeedlink = new MySqlParameter("feedlink", MySqlDbType.Text);
            sqlfeedlink.Value = HttpUtility.HtmlEncode(url);

            dbcon.Open();

            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = sql;
            dbcmd.Parameters.Add(sqlfeedlink);

            Int64 count = System.Convert.ToInt64(dbcmd.ExecuteScalar());

            if (count > 0)
            {
                status = true;
            }
        }
        catch
        {

        }
        finally
        {
            dbcon.Close();
        }

        return (status);
    }
}