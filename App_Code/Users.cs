﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;

/// <summary>
/// Summary description for Users
/// </summary>
public class Users
{
	public Users()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static bool CheckIfUserIsSubscribedToAFeed(string url, int useridx)
    {
        int feedId = GetSubscriptionIDFromURL(url);

        if (feedId == -1)
        {
            //This subscription doesn't exist for this user.  We should have added it earlier.  Time to throw exception.
            throw new Exception("Unable to find subscription for URL " + url);
        }

        //see if this subscription id exists in usersubs table for this user
        string sql = "SELECT COUNT(id) FROM usersubs WHERE idsid='" + useridx + "' AND subscriptionid='" + feedId + "'";
        IDbConnection dbcon = new MySqlConnection(ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString);
        
        int subsCount = -1;
        try
        {    
            dbcon.Open();

            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = sql;

            subsCount = (int)dbcmd.ExecuteScalar();
        }
        catch
        {
            subsCount = -1;
        }
        finally
        {
            dbcon.Close();
        }

        if (subsCount == -1)
        {
            return (false);

        } //no exception a subscription is found that matches the URL
        return (true);
    }

    public static bool insertUserIntoDB(string userid, string friendlyname, string email, string details)
    {
        bool status = false;

        //Do some input checking
        if (userid == null || String.IsNullOrEmpty(userid) || friendlyname == null || details == null)
        {
            return status;
        }

        string connectionString = ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString;
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);

        try
        {
            string sql = "INSERT INTO users VALUES (DEFAULT, @userid, @friendlyname, @useremail, @details)";
            
            
            MySqlParameter sqluserid = new MySqlParameter("userid", MySqlDbType.Text);
            sqluserid.Value = HttpUtility.HtmlEncode(userid);

            MySqlParameter sqlfriendlyname = new MySqlParameter("friendlyname", MySqlDbType.Text);
            sqlfriendlyname.Value = HttpUtility.HtmlEncode(friendlyname);

            MySqlParameter sqlemail = new MySqlParameter("useremail", MySqlDbType.Text);
            sqlemail.Value = HttpUtility.HtmlEncode(email);

            MySqlParameter sqldetails = new MySqlParameter("details", MySqlDbType.Text);
            sqldetails.Value = HttpUtility.HtmlEncode(details);


            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = sql;
            dbcmd.Parameters.Add(sqluserid);
            dbcmd.Parameters.Add(sqlfriendlyname);
            dbcmd.Parameters.Add(sqlemail);
            dbcmd.Parameters.Add(sqldetails);


            dbcon.Open();
            string cmd = dbcmd.CommandText;
            dbcmd.ExecuteNonQuery();

            status = true;
        }
        catch
        {
            status = false;
        }
        finally
        {
            dbcon.Close();
        }

        return (status);
    }

    public static string getSubscriptions(int useridx)
    {

        string html = "";

        string connectionString = ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString;
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);

        try
        {
            string sql = "SELECT s.title, s.link, s.description, s.guid, s.enclosure FROM subscriptions s ";
            sql += "INNER JOIN usersubs u ON s.id=u.id WHERE u.userid=@useridx";

            /*       SELECT   m.account_number
           ,        m.member_type         -- A fk to common_lookup table.
           ,        m.credit_card_number
           ,        m.credit_card_type    -- A fk to common_lookup table.
           ,        c.first_name
           ,        c.middle_name
           ,        c.last_name
           ,        c.contact_type        -- A fk to common_lookup table.
           FROM     member m INNER JOIN contact c
           ON       m.member_id = c.member_id
           WHERE    c.first_name = 'Harry'
           AND      c.middle_name = 'James'
           AND      c.last_name = 'Potter'\G */


            MySqlParameter param = new MySqlParameter("useridx", MySqlDbType.Int16);
            param.Value = useridx;



            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = sql;
            dbcmd.Parameters.Add(param);


            dbcon.Open();
            IDataReader reader = dbcmd.ExecuteReader();


            while (reader.Read())
            {

                html += makeSubscriptionItemHtml(reader["title"].ToString(), reader["link"].ToString(), reader["description"].ToString(), reader["guid"].ToString(), reader["enclosure"].ToString());
            }
        }
        catch
        {

        }
        finally
        {
            dbcon.Close();
        }

        return (html);

    }

    public static string makeSubscriptionItemHtml(string title, string link, string description, string guid, string enclosure)
    {
        string html = "";


        html += "<p>";
        html += "title = " + HttpUtility.HtmlDecode(title) + "<br />";
        html += " link = " + makeLink(HttpUtility.HtmlEncode(link), "link") + "<br />";
        //html += " weblink = " + makeLink(weblink, weblink);
        html += " description = " + HttpUtility.HtmlDecode(HttpUtility.HtmlEncode(description)) + "<br />";
        html += " guid = " + HttpUtility.HtmlDecode(guid) + "<br />";
        html += " Enclosure = " + HttpUtility.HtmlEncode(enclosure) + "<br />";
        html += "--------------------------------------------------------------------------------------------------</p>\n";


        return (html);


    }

    public static string makeLink(string url, string title)
    {
        return ("<a href=\"" + url + "\">" + title + "</a>");
    }


    public static bool doesUserExistInDB(string userid)
    {
        bool status = false;

        //input checking
        if (userid == null)
        {
            return false;
        }

        string connectionString = ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString;
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);

        try
        {
            string sql = "SELECT COUNT(userid) from users where userid=@userid";
            MySqlParameter param = new MySqlParameter("userid", MySqlDbType.Text);
            param.Value = HttpUtility.HtmlEncode(userid);

            dbcon.Open();

            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = sql;
            dbcmd.Parameters.Add(param);

            IDataReader reader = dbcmd.ExecuteReader();

            long count = 0;
            while (reader.Read())
            {
                count = System.Convert.ToInt64(reader[0]);
            }

            if (count > 0)
            {
                status = true;
            }
        }
        catch
        {

        }
        finally
        {
            dbcon.Close();
        }
        return (status);
    }

    public static int getUserIdxFromDB(string username)
    {
        int id = 0;


        //input checking
        if (username == null)
        {
            return -1;
        }

        string connectionString = ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString;
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);

        try
        {
            string sql = "SELECT id from users where userid=@userid";
            MySqlParameter param = new MySqlParameter("userid", MySqlDbType.Text);
            param.Value = HttpUtility.HtmlEncode(username);

            dbcon.Open();

            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = sql;
            dbcmd.Parameters.Add(param);

            IDataReader reader = dbcmd.ExecuteReader();


            while (reader.Read())
            {
                id = System.Convert.ToInt32(reader["id"]);
            }
        }
        catch
        {

        }
        finally
        {
            dbcon.Close();
        }


        return (id);
    }

    public static void AddFeedToUserSubscriptions(string url, int useridx)
    {
        int subsID = GetSubscriptionIDFromURL(url);

        string sql = "INSERT INTO usersubs VALUES(DEFAULT, '" + useridx + "','" + subsID + "')";

        int numRows = RunSQLNoQuery(sql);
    }

    private static int RunSQLNoQuery(string sql)
    {
        int numRows = -1;

        IDbConnection dbcon = new MySqlConnection(ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString);

        try
        {

            dbcon.Open();

            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = sql;

            numRows = dbcmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            dbcon.Close();
        }

        return (numRows);

    }

    private static int GetSubscriptionIDFromURL(string url)
    {
        string connectionString = ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString;
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);

        //Get subs ID from DB based on URL
        string sql = "SELECT id FROM subscriptions WHERE feedlink='" + HttpUtility.HtmlEncode(url) + "'";

        int feedId = -1;

        try
        {

            dbcon.Open();

            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = sql;
            feedId = Convert.ToInt32(dbcmd.ExecuteScalar());
        }
        catch
        {
            feedId = -1;
        }
        finally
        {
            dbcon.Close();
        }

        return (feedId);
    }
}