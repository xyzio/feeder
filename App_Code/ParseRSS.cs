﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Argotic.Common;
using Argotic.Syndication;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
//using Microsoft.Security.Application;

/// <summary>
/// Summary description for ParseRSS
/// </summary>
public class ParseRSS
{
    public ParseRSS()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string getItems(int subscriptionID)
    {
        string html = "";

        string title = "";
        string link = "";
        string description = "";
        string guid = "";
        string enclosure = "";


        string connectionString = ConfigurationManager.ConnectionStrings["mySqlConn"].ConnectionString;
        IDbConnection dbcon;
        dbcon = new MySqlConnection(connectionString);
     
        string sql = "SELECT feedlink FROM subscriptions WHERE id='" + subscriptionID + "'";

        try
        {
            string feedlink = "";
            
            dbcon.Open();

            IDbCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = sql;

            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                feedlink = (string)reader["feedlink"];
            }

            reader.Close();

            //Read feed
            GenericSyndicationFeed feed = GenericSyndicationFeed.Create(new Uri(feedlink));


          /*  foreach (GenericSyndicationItem item in feed.Items)
            {
                title = item.Title;
                description = item.Summary;
             
                //DateTime publishedOn = item.PublishedOn;

                foreach (GenericSyndicationCategory category in feed.Categories)
                {
                    string itemCategoryTerm = category.Term;
                    string itemCategoryTScheme = category.Scheme;
                }
            }*/

            if (feed.Format == SyndicationContentFormat.Rss)
            {
                RssFeed rssFeed = feed.Resource as RssFeed;
                if (rssFeed != null)
                {
                    foreach (RssItem item in rssFeed.Channel.Items)
                    {
                        // Process format specific information
                        title = item.Title;
                        description = item.Description;
                        guid = item.Guid.Value;
                        link = item.Link.ToString();
                        enclosure = "";
                        foreach (RssEnclosure closure in item.Enclosures)
                        {
                            enclosure += closure.Url.ToString();
                        }
                        html += Users.makeSubscriptionItemHtml(title, link, description, guid, enclosure);
                    }
                }
            }
            if (feed.Format == SyndicationContentFormat.Atom)
            {
                AtomFeed atomFeed = feed.Resource as AtomFeed;

                if (atomFeed != null)
                {
                    foreach (AtomEntry item in atomFeed.Entries)
                    {
                        title = item.Title.Content;
                        description = item.Summary.Content;
                        guid = item.Id.ToString();

                        foreach (AtomLink alink in item.Links)
                        {
                            link = alink.Uri.ToString();
                            break; //Just get first link
                        }

                        html += Users.makeSubscriptionItemHtml(title, link, description, guid, enclosure);
                    }
                }
            }

        }
        catch
        {
        }
        finally
        {
            dbcon.Close();
        }

        return (html);
    }







}
